import React from 'react';
import { node } from 'prop-types';

function Row(props) {
  const { iconCell, leftCell, rightCell } = props;

  return (
    <div className="row">
      <div className="icon">{iconCell}</div>
      <div className="line">
        <div className="left">{leftCell}</div>
        <div className="right">{rightCell}</div>
      </div>
    </div>
  );
}

Row.propTypes = {
  iconCell: node,
  leftCell: node,
  rightCell: node
};

export default Row;
