import React from 'react';
import { string, func, number, node } from 'prop-types';

function HyperLink(props) {
  const { to, handleClick, size, children } = props;

  return (
    <a href={to} onClick={handleClick} style={{ fontSize: size }}>
      {children}
    </a>
  );
}

HyperLink.defaultProps = {
  to: '#',
  size: 16
};

HyperLink.propTypes = {
  to: string,
  handleClick: func,
  size: number,
  children: node
};

export default HyperLink;
