import React from 'react';
import { string, oneOfType, number, func, bool, node } from 'prop-types';

function TextField(props) {
  const { type, placeholder, value, handleChange, size, editable, children } = props;

  return editable ? (
    <input type={type} placeholder={placeholder} defaultValue={value} onChange={handleChange} style={{ fontSize: size }} />
  ) : (
    <p style={{ fontSize: size }}>{children}</p>
  );
}

TextField.defaultProps = {
  type: 'text',
  size: 16,
  editable: false
};

TextField.propTypes = {
  type: string,
  placeholder: string,
  value: oneOfType([string, number]),
  handleChange: func,
  size: number,
  editable: bool,
  children: node
};

export default TextField;
