import React from 'react';
import { number, string, node } from 'prop-types';

function Block(props) {
  const { height, color, children } = props;

  return (
    <div className="block" style={{ height }}>
      <div className="frame" />
      <div className="content" style={{ background: color }}>
        {children}
      </div>
    </div>
  );
}

Block.defaultProps = {
  height: 60,
  color: '#ff7043'
};

Block.propTypes = {
  height: number,
  color: string,
  children: node
};

export default Block;
