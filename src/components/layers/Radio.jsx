import React, { useState } from 'react';
import { string, func } from 'prop-types';

function Radio(props) {
  const [checked, setChecked] = useState(true);

  const onClick = () => {
    const { handleClick } = props;

    if (handleClick) {
      handleClick();
    }

    setChecked(!checked);
  };

  const { color } = props;

  return (
    <svg fill="none" width={40} height={25} onClick={onClick} style={{ cursor: 'pointer' }}>
      {checked && <circle cx={20} cy={12.5} r={5} fill={color} />}
      <circle cx={20} cy={12.5} r={10} stroke={color} strokeWidth={3} />
    </svg>
  );
}

Radio.defaultProps = {
  color: '#212121'
};

Radio.propTypes = {
  color: string,
  handleClick: func
};

export default Radio;
