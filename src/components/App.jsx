import React, { useState, useEffect } from 'react';
import { readAllPizzas } from '../services/pizza';
import { generateId } from '../utils';
import { List, Info, Add } from './containers';

function useArray(initialState) {
  const [value, setValue] = useState(initialState);

  const addValue = newValue => {
    setValue([...value, { id: generateId(), ...newValue }]);
  };

  const upValue = currentValue => {
    setValue(value.map(v => (v.id === currentValue.id ? currentValue : v)));
  };

  const delValue = id => {
    setValue(value.filter(v => v.id !== id));
  };

  return [value, setValue, addValue, upValue, delValue];
}

function App() {
  const [pizzas, setPizzas, addToPizzas, upToPizzas, delToPizzas] = useArray([]);
  const [pizza, setPizza] = useState(null);

  useEffect(() => {
    if (pizzas.length === 0) {
      readAllPizzas().then(result => {
        setPizzas(result);
      });
    }
  });

  return (
    <>
      {!pizza && <List pizzas={pizzas} setPizza={setPizza} />}
      {pizza && pizza.id && <Info pizza={pizza} upToPizzas={upToPizzas} delToPizzas={delToPizzas} resetPizza={() => setPizza(null)} />}
      {pizza && !pizza.id && <Add pizza={pizza} addToPizzas={addToPizzas} resetPizza={() => setPizza(null)} />}
    </>
  );
}

export default App;
