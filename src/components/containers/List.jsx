import React, { useState, useEffect } from 'react';
import { arrayOf, shape, string, number, func } from 'prop-types';
import { Block, Row, HyperLink, TextField } from '../layers';
import { lo, scrollToListView } from '../../utils';

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [value, event => setValue(event.target.value)];
}

function List(props) {
  const [filter, setFilter] = useInput('');

  useEffect(() => {
    setTimeout(() => {
      scrollToListView();
    }, 1000);
  }, []);

  const byLabel = ({ label }) => lo(label).includes(lo(filter));

  const byKey = key => (a, b) => (a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0);

  const { pizzas, setPizza } = props;

  return (
    <div id="app">
      <Block color="#ef5350">
        <TextField placeholder="All U Need Is Pizza" handleChange={setFilter} size={18} editable />
      </Block>
      <Block id="pizzas" height={285} color="#ffca28">
        <div className="listview">
          <Row leftCell={<HyperLink handleClick={() => setPizza({ label: '', items: [''], price: 0 })}>New</HyperLink>} />
          {pizzas.length > 0 &&
            pizzas
              .filter(byLabel)
              .sort(byKey('label'))
              .map(({ id, label, items, price }) => (
                <Row
                  key={id}
                  leftCell={<HyperLink handleClick={() => setPizza({ id, label, items, price })}>{label}</HyperLink>}
                  rightCell={<TextField>{price.toFixed(2)} €</TextField>}
                />
              ))}
          <Row leftCell={<HyperLink to="//gitlab.com/aunip/react">Code Source</HyperLink>} />
        </div>
      </Block>
      <Block color="#ffa726">
        <TextField size={18}>
          {pizzas.filter(byLabel).length} / {pizzas.length}
        </TextField>
      </Block>
    </div>
  );
}

List.defaultProps = {
  pizzas: []
};

List.propTypes = {
  pizzas: arrayOf(
    shape({
      id: string,
      label: string,
      items: arrayOf(string),
      price: number
    })
  ),
  setPizza: func
};

export default List;
