import React, { useState } from 'react';
import { shape, string, arrayOf, number, func } from 'prop-types';
import { Block, Row, HyperLink, TextField, Radio } from '../layers';
import { capitalize } from '../../utils';

function useLocked(initialState) {
  const [value, setValue] = useState(initialState);

  return [value, () => setValue(!value)];
}

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [value, event => setValue(event.target.value)];
}

function useArray(initialState) {
  const [values, setValues] = useState(initialState);

  return [values, index => event => setValues(values.map((value, idx) => (idx === index ? event.target.value : value)))];
}

function Info(props) {
  const {
    pizza: { id, ...info }
  } = props;

  const [takeOff, setTakeOff] = useState([]);
  const [locked, setLocked] = useLocked(true);
  const [label, setLabel] = useInput(info.label);
  const [items, setItem] = useArray(info.items);
  const [price, setPrice] = useInput(info.price);

  const handleTakeOff = item => {
    if (takeOff.find(i => i === item)) {
      setTakeOff(takeOff.filter(i => i !== item));
    } else {
      setTakeOff([...takeOff, item]);
    }
  };

  const removePizza = () => {
    const { delToPizzas, resetPizza } = props;

    delToPizzas(id);
    resetPizza();
  };

  const updatePizza = () => {
    if (!locked) {
      props.upToPizzas({
        id,
        label: capitalize(label),
        items,
        price: parseFloat(price)
      });
    }

    setLocked();
  };

  return (
    <div id="app">
      <Block color="#ef5350">
        <TextField value={label} placeholder="Label" handleChange={setLabel} size={18} editable={!locked}>
          {label}
        </TextField>
      </Block>
      <Block height={285} color="#ffca28">
        <div className="listview">
          {items.length > 0 &&
            items.map((item, idx) => (
              <Row
                key={idx}
                iconCell={<Radio handleClick={() => handleTakeOff(item)} />}
                leftCell={
                  <TextField value={item} placeholder="Item" handleChange={setItem(idx)} editable={!locked}>
                    {item}
                  </TextField>
                }
              />
            ))}
          <Row
            rightCell={
              <TextField type="number" value={price} placeholder="Price" handleChange={setPrice} editable={!locked}>
                {(price - takeOff.length).toFixed(2)} €
              </TextField>
            }
          />
        </div>
      </Block>
      <Block color="#ffa726">
        <HyperLink handleClick={updatePizza} size={18}>
          {locked ? 'Unlock' : 'Lock'}
        </HyperLink>
        <HyperLink handleClick={props.resetPizza} size={18}>
          Back
        </HyperLink>
        {!locked && (
          <HyperLink handleClick={removePizza} size={18}>
            Del
          </HyperLink>
        )}
      </Block>
    </div>
  );
}

Info.propTypes = {
  pizza: shape({
    id: string,
    label: string,
    items: arrayOf(string),
    price: number
  }),
  upToPizzas: func,
  delToPizzas: func,
  resetPizza: func
};

export default Info;
