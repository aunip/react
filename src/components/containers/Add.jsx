import React, { useState } from 'react';
import { shape, string, arrayOf, number, func } from 'prop-types';
import { Block, Row, HyperLink, TextField, Radio } from '../layers';
import { capitalize } from '../../utils';

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [value, event => setValue(event.target.value)];
}

function Add(props) {
  const { pizza } = props;

  const [label, setLabel] = useInput(pizza.label);
  const [items, setItems] = useState(pizza.items);
  const [price, setPrice] = useInput(pizza.price);

  const addItem = index => event => {
    const value = event.target.value;

    const newItems = items.map((item, idx) => (idx === index ? value : item));

    if (value.length > 0) {
      if (items[index + 1] === undefined) {
        setItems([...newItems, '']);
      } else {
        setItems(newItems);
      }
    } else {
      setItems(newItems.filter((_, idx) => idx !== index + 1));
    }
  };

  const isValid = () => {
    const allItems = items.filter(item => item.length > 0);

    return label.length > 0 && allItems.length > 0 && price.length > 0;
  };

  const addNewPizza = () => {
    const { addToPizzas, resetPizza } = props;

    if (isValid()) {
      addToPizzas({
        label: capitalize(label),
        items: items.filter(item => item.length > 0),
        price: parseFloat(price)
      });

      resetPizza();
    }
  };

  return (
    <div id="app">
      <Block color="#ef5350">
        <TextField placeholder="Label" value={label} handleChange={setLabel} size={18} editable />
      </Block>
      <Block height={285} color="#ffca28">
        <div className="listview">
          {items.map((item, idx) => (
            <Row
              key={idx}
              iconCell={<Radio color={item.length > 0 ? '#212121' : '#fafafa'} />}
              leftCell={<TextField placeholder="Item" value={item} handleChange={addItem(idx)} editable />}
            />
          ))}
          <Row rightCell={<TextField type="number" placeholder="Price" value={price} handleChange={setPrice} editable />} />
        </div>
      </Block>
      <Block color="#ffa726">
        {isValid() && (
          <HyperLink handleClick={addNewPizza} size={18}>
            Save
          </HyperLink>
        )}
        <HyperLink handleClick={props.resetPizza} size={18}>
          Back
        </HyperLink>
      </Block>
    </div>
  );
}

Add.defaultProps = {
  pizza: {
    label: '',
    items: [''],
    price: 0
  }
};

Add.propTypes = {
  pizza: shape({
    label: string,
    items: arrayOf(string),
    price: number
  }),
  addToPizzas: func,
  resetPizza: func
};

export default Add;
